package net.rstvvoli.moveFiles;

import java.io.IOException;
import java.nio.file.*;

public class MoveFiles {

    private static final String INVALID_PATH_ERR = "Ошибка указания пути.";
    private static final String IO_EXCEPTION_ERR = "Ошибка ввода-вывода.";
    private static final String INFO = "Вывод информации о перемещённом каталоге: ";
    private static final String MOVED = "Перемещённые файлы: ";


    public static void main(String[] args) {
        move(EnterPaths.enterSource(), EnterPaths.enterTarget());
    }

    /**
     * Метод осуществляет перемещение каталога
     * @param source путь к каталогу-источнику;
     * @param target путь к каталогу-приёмнику;
     */
    private static void move(String source, String target) {
        try {
            /*
            Создается два объекта класса Path, для каталога-источника и каталога-приёмника, с использованием метода
            get() класса Paths. Данный метод принимает строку, содержащую путь;
             */
            Path sourceDirectory = Paths.get(source), targetDirectory = Paths.get(target);

            /*
            Перемещение производится в следствие вызова статического метода класса Files - move;
            В параметры метода передаются обхекты Path;
            StandardCopyOption.REPLACE_EXISTING - параметр перезаписи файла;
             */
            Files.move(sourceDirectory, targetDirectory, StandardCopyOption.REPLACE_EXISTING);
            infoAboutMovedFiles(sourceDirectory,targetDirectory);

        } catch (InvalidPathException e) {
            System.out.println(INVALID_PATH_ERR);
        } catch (IOException e) {
            System.out.println(IO_EXCEPTION_ERR);
        }
    }

    /**
     * Метод предназначен для предоставления информации о перемещённом каталоге;
     * @param sourceDirectory каталог-источник;
     * @param targetDirectory каталог-приёмник;
     * @throws IOException ошибка ввода/вывода;
     */
    private static void infoAboutMovedFiles(Path sourceDirectory, Path targetDirectory) throws IOException {
        System.out.println(INFO);
        System.out.println("Каталог-источник: " + sourceDirectory.getFileName());
        System.out.println("Каталог-приёмник: " + targetDirectory.getFileName());
        System.out.println();
        System.out.println(MOVED);

        int sumMovedFiles = 0;
        DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(String.valueOf(targetDirectory)));
            for (Path file : directoryStream) {
                System.out.println("\t" + file.getFileName());
                sumMovedFiles++;
            }
        System.out.println("Количество перемещённых файлов: " + sumMovedFiles);
    }
}