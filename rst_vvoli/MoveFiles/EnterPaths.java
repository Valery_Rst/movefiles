package net.rstvvoli.moveFiles;

import java.util.Scanner;

class EnterPaths {

    private static final String SRC_SC = "Введите путь к каталогу-источнику: ";
    private static final String TRG_SC = "Введите путь к каталогу-приёмнику: ";
    private static Scanner scanner = new Scanner(System.in);

    static String enterSource() {
        System.out.println(SRC_SC);
        return  scanner.nextLine();
    }

    static String enterTarget() {
        System.out.println(TRG_SC);
        return scanner.nextLine();
    }
}